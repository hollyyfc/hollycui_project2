[![pipeline status](https://gitlab.com/hollyyfc/hollycui_project2/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_project2/-/commits/main)

# 🐳 Holly's Rust Actix Web App with Docker 2.0

## Description

This project is an **improved version** of my [web-based **Rock-Paper-Scissors** game](https://gitlab.com/hollyyfc/hollycui_docker), developed in [Rust](https://www.rust-lang.org/) with the [Actix web](https://actix.rs/) framework. It features a user-friendly interface for playing against the computer, with game outcomes dynamically generated and displayed. Containerized with [Docker](https://www.docker.com/products/docker-desktop/) for easy deployment, the game illustrates the effective use of Rust for web development and Docker for application distribution.

In the 2.0 version, you will expect a better UI experience with more effective data communication and flavors added via Emojis and playful interactive choices. 

## Demo

Click on the image and get redirected to YouTube for watching: 

[![Project Demo](http://img.youtube.com/vi/AHDbEWOtdV8/0.jpg)](http://www.youtube.com/watch?v=AHDbEWOtdV8)

## Steps Walkthrough

- **Build Cargo Project** 
    - `cargo new <YOUR-PROJECT-NAME>` in desired directory
    - Add `actix-web`, `actix-files`, and relative dependencies in `Cargo.toml`
    - Build Rust functions in `src/main.rs` implementing Actix structures 
    - Add customized HTML files (`index.html` & `results.html`) for webpage UI
    - `cargo run`
        - Ensure *Xcode* and *Command Line Tools* are installed or up to date!
        - `conda deactivate` if *Anaconda* is installed to environment before 💢
- **Build Dockerfile**
    - Ensure *Docker Desktop* is installed or up to date
    - Create a `Dockerfile` in project root resolving Docker image and container creation 
    - `docker build -t <YOUR-IMAGE-NAME> .` while *Docker Desktop* is open
    - `docker run -d -p 8080:8080 <YOUR-IMAGE-NAME>` for running the app on `localhost:8080`
    - `docker ps` for inspecting the running containers 
    - `docker stop <CONTAINER-ID>` for stopping a container with `CONTAINER-ID` retrieved from last step
    - `docker rm <CONTAINER-ID>` for removing the container from using up undesired resources 

