use actix_files as fs;
use std::fs::read_to_string;
use actix_web::{web, App, HttpServer, HttpResponse, Result};
use rand::seq::SliceRandom;
use std::sync::Mutex;

struct AppState {
    game_choice: Mutex<Vec<&'static str>>,
}

fn choice_to_emoji(choice: &str) -> &str {
    match choice {
        "rock" => "✊",
        "paper" => "🖐️",
        "scissors" => "✌️",
        _ => "",
    }
}

async fn play_game(query: web::Query<std::collections::HashMap<String, String>>, data: web::Data<AppState>) -> Result<HttpResponse> {
    let user_choice = query.get("choice").expect("No choice made!");
    let game_choices = data.game_choice.lock().unwrap();
    let server_choice = game_choices.choose(&mut rand::thread_rng()).unwrap();

    let outcome = match (*server_choice, user_choice.as_str()) {
        ("rock", "scissors") | ("scissors", "paper") | ("paper", "rock") => "You lose!",
        (c, u) if c == u => "It's a tie!",
        _ => "You win!",
    };

    let user_choice_emoji = choice_to_emoji(user_choice);
    let computer_choice_emoji = choice_to_emoji(server_choice);

    let mut html_content = read_to_string("results.html").expect("Unable to read HTML template");
    html_content = html_content.replace("{{user_choice}}", user_choice_emoji);
    html_content = html_content.replace("{{computer_choice}}", computer_choice_emoji);
    html_content = html_content.replace("{{game_result}}", outcome);

    Ok(HttpResponse::Ok().content_type("text/html").body(html_content))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_data = web::Data::new(AppState {
        game_choice: Mutex::new(vec!["rock", "paper", "scissors"]),
    });

    HttpServer::new(move || {
        App::new()
            .app_data(app_data.clone())
            .service(web::resource("/").route(web::get().to(|| async { fs::NamedFile::open("index.html") })))
            .route("/play", web::get().to(play_game))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}